public class JavaStrings12 {
     public static void main(String[] args) {
        String x = "10";
        int y = 20;
        String z = x + y;   // z will be 1020 (a String)
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);  
    }
}
